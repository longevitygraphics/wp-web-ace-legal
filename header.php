<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title(''); ?></title>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<div class="body-bag">

		    <nav class="accessibility-nav">
		      <ol>
		        <li><a href="#navigation">Skip to navigation</a></li>
		        <li><a href="#content">Skip to content</a></li>
		      </ol>
		    </nav>
		    <!-- / accessibility-nav -->
		    <?php
		    	$custom_logo_id = get_theme_mod( 'custom_logo' );
				$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
			?>

		    <header class="navbar navbar-default">
		        <!--<?php echo do_shortcode('[social-media-icons class="hidden-xs"]'); ?>-->
		        <div class="top-nav-cta"><div><span><i class="fa fa-phone" aria-hidden="true"></i> Call us Today</span> <a href="tel:6045647077">(604) 564-7077</a></div></div>

		        <div class="navbar-header">
		          
		          <div class="navbar-brand">
		            <a class="site-name" href="<?php echo home_url(); ?>" rel="index" title="Go to homepage"><img src="<?php echo $image[0]; ?>" alt=""></a>
		          </div>

					<div class="mobile-nav">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
				          <span class="sr-only">Toggle navigation</span>
				          <span class="icon-bar"></span>
				          <span class="icon-bar"></span>
				          <span class="icon-bar"></span>
				        </button>
					</div>
		        </div>

		        <nav id="navigation" class="collapse navbar-collapse" role="navigation">
		          <h6 class="sr-only">Main Navigation</h6>
		          <?php acelegal_main_nav(); ?>
		        </nav>
		    </header>
		    <!-- / header -->

			<main class="content" id="content">