<?php
ob_start();

// we're firing all out initial functions at the start
add_action('after_setup_theme','acelegal_start', 16);

function acelegal_start() {

    // launching operation cleanup
    add_action('init', 'acelegal_head_cleanup');
    // remove WP version from RSS
    add_filter('the_generator', 'acelegal_rss_version');
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'acelegal_remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action('wp_head', 'acelegal_remove_recent_comments_style', 1);
    // clean up gallery output in wp
    add_filter('gallery_style', 'acelegal_gallery_style');

    // enqueue base scripts and styles
    add_action('wp_enqueue_scripts', 'acelegal_scripts_and_styles', 999);
    // ie conditional wrapper

    // launching this stuff after theme setup
    acelegal_theme_support();

    // adding sidebars to Wordpress (these are created in functions.php)
    add_action( 'widgets_init', 'acelegal_register_sidebars' );
    // adding the acelegal search form (created in functions.php)
    add_filter( 'get_search_form', 'acelegal_wpsearch' );

    // cleaning up random code around images
    add_filter('the_content', 'acelegal_filter_ptags_on_images');
    // cleaning up excerpt
    add_filter('excerpt_more', 'acelegal_excerpt_more');

} /* end acelegal start */

/*********************
WP_HEAD GOODNESS
The default wordpress head is a mess.
Let's clean it up by removing all the junk we don't need.
*********************/

function acelegal_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
} /* end acelegal head cleanup */

// remove WP version from RSS
function acelegal_rss_version() { return ''; }

// remove injected CSS for recent comments widget
function acelegal_remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function acelegal_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// remove injected CSS from gallery
function acelegal_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function acelegal_scripts_and_styles() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {
    $theme_version = wp_get_theme()->Version;

	// removes WP version of jQuery
	wp_deregister_script('jquery');

	// jQuery
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/_assets/js/jquery.min.js', array(), '1.11.2', false );

    // modernizr
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/_assets/js/modernizr.min.js', array(), '2.8.3', false );

    // adding plugins scripts file in the footer
    wp_enqueue_script( 'plugins-js', get_template_directory_uri() . '/_assets/js/plugins.min.js', array( 'jquery' ), $theme_version, true );

    // adding main scripts file in the footer
    wp_enqueue_script( 'acelegal-js', get_template_directory_uri() . '/_assets/js/main.js', array( 'jquery' ), $theme_version, true );

    //website script
    wp_enqueue_script( 'script-js', get_template_directory_uri() . '/src/js/lg-scripts.js', array( 'jquery' ), $theme_version, true );

    // register lato font stylesheet
    wp_enqueue_style( 'lato-font', '//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic', array(), $theme_version, 'all' );

    // register main stylesheet
    wp_enqueue_style( 'acelegal-stylesheet', get_template_directory_uri() . '/_assets/css/main.css', array(), $theme_version, 'all' );

    wp_enqueue_style( 'acelegal-stylesheet2', get_template_directory_uri() . '/style.css', array(), $theme_version, 'all' );

    wp_enqueue_style( 'font-awsome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), $theme_version, 'all' );

    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

  }
}

/*********************
THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function acelegal_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support('post-thumbnails');

	// rss
	add_theme_support('automatic-feed-links');

	//custom logo

	add_theme_support( 'custom-logo', array(
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// wp menus
	add_theme_support( 'menus' );

	//html5 support (http://themeshaper.com/2013/08/01/html5-support-in-wordpress-core/)
	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	function add_svg_to_upload_mimes( $upload_mimes ) {
		$upload_mimes['svg'] = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';
		return $upload_mimes;
	}
	add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );


} /* end acelegal theme support */

// Add image sizes
add_image_size( 'homepage', 1500, 600, true );
add_image_size( 'cta', 239, 206, true );


/*********************
Site Options
*********************/

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}


/* Re-enable the links manager */
add_filter( 'pre_option_link_manager_enabled', '__return_true' );

/* Show links */
function dee_display_bookmark_links() {
	$args = array(
		'order'            => 'DESC',
		'show_description' => 1,
		'category_orderby' => 'ID',
		'category_order' => 'DESC',
		'category_before'  => '',
		'category_after'   => '' );
ob_start();
	wp_list_bookmarks( $args );
return ob_get_clean();
}
add_shortcode( 'resources', 'dee_display_bookmark_links' );

/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function acelegal_page_navi($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
	echo $before.'<nav class="page-navigation"><ul class="pagination">'."";
	if ($start_page >= 2 && $pages_to_show < $max_page) {
		$first_page_text = __( "First", 'acelegaltheme' );
		echo '<li><a href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
	}
	echo '<li>';
	previous_posts_link('<<');
	echo '</li>';
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="current"><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li>';
	next_posts_link('>>');
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = __( "Last", 'acelegaltheme' );
		echo '<li><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
	}
	echo '</ul></nav>'.$after."";
} /* end page navi */

/*********************
ADD FOUNDATION FEATURES TO WORDPRESS
*********************/
// Add "has-dropdown" CSS class to navigation menu items that have children in a submenu.
function nav_menu_item_parent_classing( $classes, $item )
{
    global $wpdb;

    if (
        !property_exists( $item, 'classes' )
        || !is_array( $item->classes )
    ) {
        return $classes;
    }

    $has_children = in_array( 'menu-item-has-children', $item->classes );

    if ( $has_children ) {
        array_push( $classes, "dropdown" );
    }

    return $classes;
}

add_filter( "nav_menu_css_class", "nav_menu_item_parent_classing", 10, 2 );

//Deletes empty classes and changes the sub menu class name
    function change_submenu_class($menu) {
        $menu = preg_replace('/ class="sub-menu"/',' class="dropdown-menu"',$menu);
        return $menu;
    }
    add_filter ('wp_nav_menu','change_submenu_class');


//Use the active class of the ZURB Foundation for the current menu item. (From: https://github.com/milohuang/reverie/blob/master/functions.php)
function required_active_nav_class( $classes, $item ) {
    if ( $item->current == 1 || $item->current_item_ancestor == true ) {
        $classes[] = 'active';
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'required_active_nav_class', 10, 2 );

// Search Form
function acelegal_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __('Search for:', 'acelegaltheme') . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search the Site...','acelegaltheme').'" />
	<input type="submit" id="searchsubmit" class="button" value="'. esc_attr__('Search') .'" />
	</form>';
	return $form;
} // don't remove this bracket!

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs
function acelegal_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function acelegal_excerpt_more($more) {
	global $post;
	// edit here if you like
return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __('Read', 'acelegaltheme') . get_the_title($post->ID).'">'. __('Read more &raquo;', 'acelegaltheme') .'</a>';
}

//  Stop WordPress from using the sticky class (which conflicts with Foundation), and style WordPress sticky posts using the .wp-sticky class instead
function remove_sticky_class($classes) {
	$classes = array_diff($classes, array("sticky"));
	$classes[] = 'wp-sticky';
	return $classes;
}
add_filter('post_class','remove_sticky_class');

/*
 * This is a modified the_author_posts_link() which just returns the link.
 *
 * This is necessary to allow usage of the usual l10n process with printf().
 */
function acelegal_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}


/*********************
MENUS & NAVIGATION
*********************/
// REGISTER MENUS
register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu' )   // main nav in header
	)
);


// THE MAIN MENU
function acelegal_main_nav() {
    wp_nav_menu(array(
    	'container' => false,                           // remove nav container
    	'container_class' => '',           // class of container (should you choose to use it)
    	'menu' => __( 'The Main Menu', 'acelegaltheme' ),  // nav name
    	'menu_class' => 'nav navbar-nav',         // adding custom nav class
    	'theme_location' => 'main-nav',                 // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
    	'fallback_cb' => 'acelegal_main_nav_fallback'      // fallback function
	));
} /* end acelegal main nav */


// HEADER FALLBACK MENU
function acelegal_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => '',      // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // before each link
        'link_after' => ''                             // after each link
	) );
}

/*********************
SIDEBARS
*********************/

// SIDEBARS AND WIDGETIZED AREAS
function acelegal_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar',
		'name' => __('Sidebar', 'acelegaltheme'),
		'description' => __('Primary sidebar.', 'acelegaltheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

} // don't remove this bracket!

/*********************
COMMENT LAYOUT
*********************/

// Comment Layout
function acelegal_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class('panel'); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix large-12 columns">
			<header class="comment-author">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<?php printf(__('<cite class="fn">%s</cite>', 'acelegaltheme'), get_comment_author_link()) ?> on
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__(' F jS, Y - g:ia', 'acelegaltheme')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'acelegaltheme'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'acelegaltheme') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!

?>