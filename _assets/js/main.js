(function($) {

  'use strict';

  var App = {

    /**
     * Init Function
     */
    init: function() {
      App.equalHeight();
    },

    /**
     * equalHeight
     */
    equalHeight: function() {
      $('.box-bottom').matchHeight();
    }

  };

  $(function() {
    App.init();
  });

})(jQuery);