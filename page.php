<?php get_header(); ?>

<?php 
	if (is_front_page()){
		get_template_part( '_partials/loop', 'front-page' );
	}else{
		get_template_part( '_partials/loop', 'internal-pages' );
	}
?>

<?php get_footer(); ?>