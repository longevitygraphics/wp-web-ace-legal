<?php
  $company_name = get_field('company_name', 'option');
  $company_email = get_field('company_email', 'option');
  $company_address = get_field('company_address', 'option');
  $company_city = get_field('company_city', 'option');
  $company_province = get_field('company_province', 'option');
  $company_postalcode = get_field('company_postalcode', 'option');
  $company_telephone = get_field('company_telephone', 'option');
  $company_fax = get_field('company_fax', 'option');
  $copyright = get_field('copyright', 'option');
  $testimonials_shortcode = get_field('testimonials_shortcode', 'option');
  $cta_title = get_field('cta_title', 'option');
  $cta_content = get_field('cta_content', 'option');
  $cta_button_label = get_field('cta_button_label', 'option');
  $cta_button_url = get_field('cta_button_url', 'option');
  $cta_image = get_field('cta_image', 'option');
  $hide_testimonials = get_post_meta(get_the_ID(), 'hide_testimonials', true);
  $hide_cta = get_post_meta(get_the_ID(), 'hide_cta', true);
  $testimonial_content = get_post_meta(get_the_ID(), 'testimonial_content', true);
  $testimonial_author = get_post_meta(get_the_ID(), 'testimonial_author', true);
  $testimonial_location = get_post_meta(get_the_ID(), 'testimonial_location', true);
?>

	<?php if ( !$hide_testimonials ) { ?>
	<div class="testimonial-section" style="background:url('<?php echo get_template_directory_uri(); ?>/src/images/testimonial-background.jpg');">
		<?php echo do_shortcode('[testimonials]'); ?>
	</div>
		
	<?php } ?>

	<!--<?php if ( $testimonial_content ) { ?>
	<div class="slider section cycle cycle--second">
		<div class="container-fluid">
			<h2 class="title h1">What people are saying</h2>
		</div>
		<div class="cycle-slideshow">
			<div class="testimonial">
				<div class="container-fluid">
					<div class="testimonial-text intro h2">
						<p><?php echo $testimonial_content; ?></p>
					</div>
					<div class="testimonial-author">
						<?php echo $testimonial_author; ?>, 
						<span class="c-second"><?php echo $testimonial_location; ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>-->

	<?php if ( !$hide_cta ) { ?>
	<div class="page-cta">
		<!--<div class="container-fluid"<?php if ( $cta_image) { ?> style="background: url(<?php echo $cta_image; ?>) right bottom no-repeat;"<?php } ?>>
			<?php if ( $cta_title ) { ?>
			<h2 class="bar-title title h1"><?php echo $cta_title; ?></h2>
			<?php } ?>
			<?php if ( $cta_button_label) { ?>
			<?php if ( $cta_button_url) { ?><a class="bar-btn btn btn-default" href="<?php echo $cta_button_url; ?>"><?php } ?><?php echo $cta_button_label; ?><?php if ( $cta_button_url) { ?></a><?php } ?>
			<?php } ?>
		</div>-->
		<div class="container">
			<div>
				<?php if ( $cta_title ): ?>
					<h2 class="bar-title title h1"><?php echo $cta_title; ?></h2>
				<?php endif; ?>
				<?php if ( $cta_content ): ?>
					<p><?php echo $cta_content; ?></p>
				<?php endif; ?>
				<?php if ( $cta_button_label) { ?>
				<?php if ( $cta_button_url) { ?><a class="cta-button" href="<?php echo $cta_button_url; ?>"><?php } ?><?php echo $cta_button_label; ?><?php if ( $cta_button_url) { ?></a><?php } ?>
				<?php } ?>
			</div>
		</div>
		<?php if($cta_image): ?>
		<div style="background: url('<?php echo $cta_image; ?>') no-repeat center center;">
		</div>
		<?php endif; ?>
	</div>
	<!-- / bar -->
	<?php } ?>

	</main>
    <!-- / content -->

		<footer class="footer">
		  <div class="container-fluid">
		    <div class="row">
		      <div class="col-xs-12 col-sm-3 col-md-3">
		        <div class="site-logo site-logo--small">
		          <?php the_custom_logo(); ?>
		        </div>
		      </div>
		      <div class="footer-col col-xs-12 col-sm-3 col-md-3 hidden-xs">
		      	<?php if ( $company_name ) { ?>
		        <span class="c-second"><?php echo $company_name; ?></span>
		        <br />
		        <?php } ?>
		        <?php if ( $company_email ) { ?>
		        <a href="mailto:<?php echo $company_email; ?>"><?php echo $company_email; ?></a>
		        <?php } ?>
		      </div>
		      <div class="footer-col col-xs-12 col-sm-3 col-md-3 hidden-xs">
		      	<?php if ( $company_address ) { ?>
		        <?php echo $company_address; ?><br />
				<?php } ?>
				<?php if ( $company_city ) { ?>
		        <?php echo $company_city; ?>, 
		        <?php } ?>
		        <?php if ( $company_province ) { ?>
		        <?php echo $company_province; ?> 
		        <?php } ?>
		        <?php if ( $company_postalcode ) { ?>
		        <?php echo $company_postalcode; ?>
		        <?php } ?>
		      </div>
		      <div class="footer-col col-xs-12 col-sm-3 col-md-3 hidden-xs">
		      	<?php if ( $company_telephone ) { ?>
		        <span class="c-second">TEL</span> <?php echo $company_telephone; ?>
		        <br />
		        <?php } ?>
		        <?php if ( $company_fax ) { ?>
		        <span class="c-second">FAX</span> <?php echo $company_fax; ?>
		        <?php } ?>
		      </div>
		      <div class="col-xs-12">
		        <p class="footer-copyright">
		          <?php if ( $copyright ) { ?><?php echo '©'.date('Y').' '.$copyright; ?><?php } ?>
		        </p>
		      </div>
		    </div>
		  </div>
		</footer>
		<!-- / footer -->

		</div>
		<!-- / container -->

		<?php wp_footer(); ?>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-62826569-1', 'auto');
			ga('send', 'pageview');
		</script>
	</body>
</html> <!-- end page -->