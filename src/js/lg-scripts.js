// Slick Slider
(function($) {
  $(document).ready(function(){
    if($(window).width() < 768){
    	$('.menu-item-has-children > a').on('click', function(e){
    		e.preventDefault();
            $(this).siblings('ul').slideDown();
    	});
    }

    //header animation
    $(window).on('scroll', function(){
        var header = $('.navbar');
        if($(window).width() > 768 && $(window).scrollTop() > 30 && !header.hasClass('transitioned')){
            header.addClass('transitioned');
        }

        if($(window).width() > 768 && $(window).scrollTop() < 30 && header.hasClass('transitioned')){
            header.removeClass('transitioned');
        }
    });
 });
}(jQuery));