<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $page_title = get_post_meta(get_the_ID(), 'page_title', true);
  $page_subtitle = get_post_meta(get_the_ID(), 'page_subtitle', true);
  $page_cta_label = get_post_meta(get_the_ID(), 'page_cta_label', true);
  $page_cta_url = get_post_meta(get_the_ID(), 'page_cta_url', true);
  $intro_title = get_post_meta(get_the_ID(), 'intro_title', true);
  $intro_paragraph = get_post_meta(get_the_ID(), 'intro_paragraph', true);
  //$intro_paragraph = the_field('intro_paragraph');
  $hide_testimonials = get_post_meta(get_the_ID(), 'hide_testimonials', true);
  $contact_form = get_field('contact_form');
  $contact_image = get_field('contact_image');
  $google_map = get_field('contact_map');

  $company_telephone = get_field('company_telephone', 'option');
  $company_email = get_field('company_email', 'option');
  $company_fax = get_field('company_fax', 'option');

  $company_address = get_field('company_address', 'option');
  $company_city = get_field('company_city', 'option');
  $company_province = get_field('company_province', 'option');
  $company_postalcode = get_field('company_postalcode', 'option');
?>

      <div class="title-bar section" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>') no-repeat center center">
        <div class="title-bar-section">
		  <?php if ( $page_title ) { ?><h2 class="title-bar-title"><?php echo $page_title; ?></h2><?php } ?>
          <?php if ( $page_subtitle ) { ?><p class="title-bar-subtitle"><?php echo $page_subtitle; ?></p><?php } ?>
        </div>
      </div>

	<div class="page-main container">
		<?php if($intro_title || $intro_paragraph): ?>
		<div class="intro-block">
			<?php if($intro_title): ?>
				<h1 class="page-headline"><?php echo $intro_title; ?></h1>
			<?php endif; ?>
			<?php if($intro_paragraph): ?>
				<p><?php echo $intro_paragraph; ?></p>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		<?php the_content(); ?>

		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<?php if($contact_form): ?>
					<?php echo do_shortcode($contact_form); ?>
				<?php endif; ?>
			</div>
			<div class="col-xs-12 col-sm-6 contact-info">	
				<?php if($contact_image): ?>
					<img class="contact-image" src="<?php echo $contact_image ?>" alt="Andrea" />
				<?php endif; ?>
				
				<div class="contact">
					<?php if($company_telephone): ?>
						Phone: <a href="tel:<?php echo $company_telephone; ?>"><?php echo $company_telephone; ?></a><br>
					<?php endif; ?>
					<?php if($company_fax): ?>
						Fax: <a href="tel:<?php echo $company_fax; ?>"><?php echo $company_fax; ?></a><br>
					<?php endif; ?>
					<?php if($company_email): ?>
						Email: <a href="mailto:<?php echo $company_email; ?>"><?php echo $company_email; ?></a><br>
					<?php endif; ?>
				</div>
			</div>
		</div>
	
		<?php if($google_map): ?>
		<div class="contact-map">
			<div class="row">
				<div class="col-md-12">
					<?php echo do_shortcode($google_map); ?>
				</div>
			</div>
			<a href="https://www.google.ca/maps/place/
			<?php if($company_address): ?>
			<?php echo $company_address ?>
			<?php endif; ?>
			<?php if($company_city): ?>
			<?php echo $company_city ?>,
			<?php endif; ?>
			<?php if($company_province): ?>
			<?php echo $company_province ?>
			<?php endif; ?>
			<?php if($company_postalcode): ?>
			<?php echo $company_postalcode ?>
			<?php endif; ?>
			">
			<i class="fa fa-map-marker" aria-hidden="true"></i>
			<?php if($company_address): ?>
			<?php echo $company_address ?>
			<?php endif; ?>
			<?php if($company_city): ?>
			<?php echo $company_city ?>,
			<?php endif; ?>
			<?php if($company_province): ?>
			<?php echo $company_province ?>
			<?php endif; ?>
			<?php if($company_postalcode): ?>
			<?php echo $company_postalcode ?>
			<?php endif; ?>
			</a>
		</div>
		<?php endif; ?>
	</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>