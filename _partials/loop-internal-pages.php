<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $page_title = get_post_meta(get_the_ID(), 'page_title', true);
  $page_subtitle = get_post_meta(get_the_ID(), 'page_subtitle', true);
  $page_cta_label = get_post_meta(get_the_ID(), 'page_cta_label', true);
  $page_cta_url = get_post_meta(get_the_ID(), 'page_cta_url', true);
  $intro_title = get_post_meta(get_the_ID(), 'intro_title', true);
  $intro_paragraph = get_post_meta(get_the_ID(), 'intro_paragraph', true);
  //$intro_paragraph = the_field('intro_paragraph');
  $hide_testimonials = get_post_meta(get_the_ID(), 'hide_testimonials', true);
  $page_headline = get_field('page_headline');
  $banner_image = get_field('banner_image');
?>

      <div class="title-bar section" style="background: url('<?php echo $banner_image; ?>') no-repeat center center">
        <div class="title-bar-section">
		  <?php if ( $page_title ) { ?><h2 class="title-bar-title"><?php echo $page_title; ?></h2><?php } ?>
          <?php if ( $page_subtitle ) { ?><p class="title-bar-subtitle"><?php echo $page_subtitle; ?></p><?php } ?>
        </div>
      </div>

	<div class="page-main container">
		<?php if($intro_title || $intro_paragraph): ?>
		<div class="intro-block">
			<?php if($intro_title): ?>
				<h1 class="page-headline"><?php echo $intro_title; ?></h1>
			<?php endif; ?>
			<?php if($intro_paragraph): ?>
				<p><?php echo $intro_paragraph; ?></p>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		<div class="primary-content">
			<?php if(has_post_thumbnail()): ?>
			<div class="featured-image">
				<div class="box box--half" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
					
				</div>
			</div>
			<?php endif; ?>
			<?php the_content(); ?>
		</div>
		
	</div>

<?php endwhile; endif; ?>