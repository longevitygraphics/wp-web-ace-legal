<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $page_title = get_post_meta(get_the_ID(), 'page_title', true);
  $page_subtitle = get_post_meta(get_the_ID(), 'page_subtitle', true);
  $page_cta_label = get_post_meta(get_the_ID(), 'page_cta_label', true);
  $page_cta_url = get_post_meta(get_the_ID(), 'page_cta_url', true);
  $intro_title = get_post_meta(get_the_ID(), 'intro_title', true);
  $intro_paragraph = get_post_meta(get_the_ID(), 'intro_paragraph', true);
  //$intro_paragraph = the_field('intro_paragraph');
  $hide_testimonials = get_post_meta(get_the_ID(), 'hide_testimonials', true);
?>

      <div class="hero cycle">
        <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-log="false" data-cycle-swipe="true" data-cycle-slides="> .cycle-el" data-cycle-timeout="0" data-cycle-prev="#hero-prev" data-cycle-next="#hero-next">
          <div class="hero-el cycle-el">
            <div class="hero-img" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
            </div>
            <div class="hero-el-cont">
            	<div>
            		<?php if ( $page_cta_label ) { ?>
	              	<?php if ( $page_cta_url ) { ?><a class="hero-cta" href="<?php echo $page_cta_url; ?>"><?php } ?><?php echo $page_cta_label; ?><?php if ( $page_cta_url ) { ?></a><?php } ?><?php } ?>
              		<?php if ( $page_title ) { ?><p class="hero-title h1"><?php echo $page_title; ?></p><?php } ?>
              		<?php if ( $page_subtitle ) { ?><p class="hero-subtitle"><?php echo $page_subtitle; ?></p><?php } ?>
              		<hr class="hero-hr">

              		<?php echo do_shortcode("[testimonial-homepagebanner]"); ?>
              		<a class="cta-button" href="/contact">Request a Consultation</a>
            	</div>
            	<!--<div class="contact-form">
            		 <h2><span>Request a </span>Consultation</h2> 
            		<?php //echo do_shortcode("[contact-form-7 id='529' title='Contact Form Homepage']"); ?>
            	</div>-->
            </div>
          </div>
        </div>
      </div>
      <!-- / hero -->


	<div>
	<?php
		$image = get_field('intro-half-image');
		$sub_title = get_field('intro-half-sub-title');
	?>
		<div class="intro-block bg-white">
			<div class="container-fluid">
		        <div class="split-content">
					<div class="half-image">
						<?php if( $image ) : ?>
							<img src="<?php echo $image ?>" alt="">
						<?php endif; ?>
					</div>
					<div class="half-copy">
						<?php if( $sub_title ) : ?>
							<h3 class="main-color"><?php echo $sub_title; ?></h3>
						<?php endif; ?>
						<?php if ( $intro_title ) { ?>
						<h2><?php echo $intro_title; ?></h2>
						<?php } ?>
						<hr>
						<?php if ( $intro_paragraph ) { ?>
				        <div><?php echo $intro_paragraph; ?></div>
				        <?php } ?>
					</div>
				</div>
			</div>
		</div>

		<?php
			$services_section_title = get_field('services_section_title');
			$services_section_sub_title = get_field('services_section_sub_title');
			$services_background_image = get_field('services_background');
		?>

		<!-- service section -->
		<div class="intro-block service-section" <?php if( $services_background_image ) : ?>style="background:url('<?php echo $services_background_image; ?>');"<?php endif; ?>>
		<div class="container-fluid">
			<?php if( $services_section_sub_title ) : ?>
				<h3 class="main-color">
					<?php echo $services_section_sub_title; ?>
				</h3>
			<?php endif; ?>
			<?php if( $services_section_title ) : ?>
				<h2 class="secondary-color">
					<?php echo $services_section_title; ?>
				</h2>
				<hr>
			<?php endif; ?>
		<?php
		// check if the repeater field has rows of data
		if( have_rows('services') ): ?>
			<div class="services">
			<?php
			// loop through the rows of data
		    while ( have_rows('services') ) : the_row(); ?>

				<div class="single-service">
					<a href="<?php the_sub_field("service_page_link"); ?>">
						<img src="<?php the_sub_field("service_item_image"); ?>" alt="">
						<h2>
							<?php the_sub_field("service_title"); ?>
						</h2>
						<p>
							<?php the_sub_field("service_description"); ?>
						</p>
					</a>
				</div>

			<?php endwhile; ?>	
			</div>
		<?php else :

			// no rows found

		endif; ?>
        	</div>
        </div>

        <!-- end service section

    	 <!-- section partner -->
		
		<div class="intro-block partner-section">
			<div>
				<?php
					$partners_section_sub_title = get_field('partners_section_sub_title');
					$partners_section_title = get_field('partners_section_title');
					$partners = get_field('partners');
				?>
				<?php if( $partners_section_sub_title ) : ?>
					<h3 class="main-color">
						<?php echo $partners_section_sub_title; ?>
					</h3>
				<?php endif; ?>
				<?php if( $partners_section_title ) : ?>
					<h2 class="secondary-color">
						<?php echo $partners_section_title; ?>
					</h2>
					<hr>
				<?php endif; ?>

				<div class="partners">
					<?php 

					if($partners){
						echo do_shortcode($partners);
					}

					?>
				</div>	

			</div>
		</div>

    <!-- end section partner -->

	</div> <!-- end section -->

<?php endwhile; endif; ?>