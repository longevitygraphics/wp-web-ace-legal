<article id="post-not-found" class="hentry">
  <header class="article-header">
    <h1><?php _e("Oops, Post Not Found!", "acelegaltheme"); ?></h1>
  </header>
  <section class="entry-content">
    <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "acelegaltheme"); ?></p>
  </section>
</article>